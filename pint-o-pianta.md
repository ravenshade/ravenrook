# Pint o' Pianta

<img src="/images/pint-o-pianta_poster.png" width="200">

A local tavern & restaurant on prime beachfront estate close to the docks.

## Supplies
 - Mugs and glassware (~30 people)
 - pewter silverware (~30 people)
 - tables, barstools, chairs (~30 people)
 - beach umbrellas
 - storage closet
 - storefront
 - plethora of large fruits
 - Blue Curacao liquor 

## Balance

**Quality Dice:** 2d12

**Revenue**: +3730g

**Debts**: -623g

| Amount      | Description               |
| ----------- | ------------------------- |
| +315g      | kai 28 earnings (2d10) | 
| +380g      | kai 27 earnings (2d12) | 
| +340g      | kai 26 earnings (2d12) | 
| +255g      | kai 25 earnings (2d12) | 
| +180g      | kai 24 earnings (2d12) | 
| +280g      | kai 23 earnings (2d12) | 
| +1270g      | kai 16-22 earnings (2d10) | 
| +180g       | kai 15 earnings (2d10)    |
| +80g        | kai 14 earnings (2d12)    |
| +60g        | Annabelle donation to Pint (refusal of earnings) |
| 180g        | kai 13 earnings (2d12) distributed to party    |
| 240g        | kai 12 earnings (2d12) distributed to party    |
| +140g       | kai 11 earnings (2d12)    |
| +315g       | kai 10 earnings (2d12)     |
| +30g        | kai 9 earnings (->2d12+)  |
| -175g       | Kai buys out Myki share   |
|             | 2d12 -> 2d10 loss         |
| +165g       | kai 8 earnings            |
| -90g        | 15x Pint o' Pianta uniforms (down payment 50%)  |
| +160g       | kai 7 earnings            |
| +165g       | kai 6 earnings            |
| +400g       | kai 5 earnings (2x)       |
| +220g       | kai 4 earnings            |
| +45g        | kai 3 earnings            |
| +120g       | kai 2 earnings            |
| +100g       | kai 1 earnings            |
| -8g         | 6g voyage (labor+galleon) to emerald isle for fruit supplies           |
| -250g       | Full storefront           |
| -50g        | Basic supplies: beach umbrellas, pewter silverware, glassware, mugs, chairs and tables       |
| -25g        | Business License for Pint o' Pianta       |
|             | +6 mugs from DT           |
| -25g        | Waterfront property       |

## Deals

| Deal        | Client              | Description               |
| ----------- | ------------------- | ------------------------- |
| -90g        | High Fashion Vendor | 15x Pint o' Pianta uniforms on delivery |

### Pint o' Pianta Distillery

 - Agreement with Annabelle to have a joint venture distillery on her farmland in exchange for profits and cheaper crops.
 - Distillery not yet built.
