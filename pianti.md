# Pianti

| Word        | Meaning     |
| ----------- | ----------- |
| ѪϪϭЭ        | Druidess, Priestess, Grovetender       |
| ҖҦӅӸѽӻҗ҈    | Honorific for greeting formally        |
| ЮѥѦ         | Honorific for salutations formally, typically with a solemn tone |
| ҸѮѭϠ        | A journey for youngling Piantas where they discover their purpose, typically involving travel.        |
| Ǽƾƴ         | demons/devils/evil-doers, often used as a curse       |
| ѮѷїҖҿ       | 'Evike' The great Pianta homeland grove, where all Piantas descended from |
| ȴȝȝȴẛ       | Pianta sprouts and trees, both singular and plural. Plural indicates a whole grove, or central garden. |
| ҉ð҂         | A family clan or lineage, an ancestry. |
| ӿӶ          | Measurement, akin to a foot, length of a pianta palm frond |


