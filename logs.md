# Logs

### Sightless Swamp
 - Pip prepared 'Swamp Soap' and swamp kits full of toiletries
 - We also prepared a keg of water
 - We have a perfect map, so we travel along with different folks 
 Pip and Malikai search for special herbs. 
 Annabelle tries to navigate and... doesn't do well.
 Jamie searches for dragons.
 Kai tells a fantastic story of romance about the queen and king of the warrior kingdom
 according to legends.
 Alexandra looking for ruins that may show archaelogical evidence of the castle.
 - Pip finds a grove of black roses, but Malikai gets chomped by a croc
 - Malikai is drugged underwater, goes unconcious, and the croc CHOMPS.
 Five Timber and Pip have an ill-fated recovery effort, barely killing the croc.
 - Malikai suffers two levels of exhausion. Feeling pain in muscles, vermin in
 muscle. 
 - The insect swarms are growing, attacking all of us. Kai, Annabelle, and Myki
 all have such a rough time with insects that their vision is blurry.
 - Annabelle, Myki, and Kai have a blinding disease. Sightrot, cured by 
 Eyebrae, an herb that rarely grows in swamp.
 - Party set camp, triple watch, Pip made insect repellent, no attacks. Malikai
 flames out his diseased limb with Myki.
 - Alex makes magical tinkering bait, party sneaks between drakes to reach the
 main castle courtyard.
 - Alex & Ramos determine that it looks like a Chromatic military base.
 Ramos knows that chromatics prefer their treasure buried deep underground.
 Gem dragons enjoy vaults on the top. Metallics like to plant bait.
 - The courtyard is covered in drakes. Five Timber goes along the wall and sets up
 ropes. 
 - Myki and Ramos fall off the rope immediately.
 - We fight off dragons, sleep one of them, the largest one escapes.
 - Myki turns into a dragon and rescues Malikai from the clutches of a dragon.
 - We get across to a side entrance. Five timber goes to scout, gets caught
 by a drake, runs out ASAP, we take down the drake.
 - The rest chase after a false path created by Five Timber, Alex, and Kai.
 - Party sneaks into castle, goes down staircases, most drakes left.
 - Find a sanctum that sunk into ground, full of water. Piles of gold, copper,
 and a stash of eggs that seem magical in nature. Found a door with 5 tiamat god
 sigils.
 - Pip gets decked by a drake sneaking in the water.
 - Despite an acid breath decimating most of the team, we were able to take
 down the drake through powerful smites.
 - Loot: 414g (in copper). 3 50gp gemstones. Citrine. Moonstone. Black Pearl (100g).
 Heavy hooded cloak. Scaled shortbow. Metallic Dragon eggs. Gold, Silver, Brass
 Bronze, Copper. 5 Swamp Eggs. Scroll with draconic writing, seems to be sending.
 - Each mouth must be given at least 15 damage of each elemental type in the same
 round (simultaneously) and the vault will open.
 - Pip took 1000 copper, transmuted it temporarily to gold, and discarded it to 
 ward off the hoard-hunting drakes following after us.
 - Party reaches our previous campsite. Kai makes potable water because we threw
 it all out in order to store more copper. Malikai and Pip make more insect 
 repellent.
 - Pip also has the eye disease. Kai and Annabelle have an improved condition.
 - Dragon crackling at night. Malikai and Five Timber check on it, Malikai guesses
 it could be anywhere between hours to months. They just go to sleep.
 - Party gets back to ravenrook safe and sound.

### Toil, Toil, Boy we're in Trouble
Session 3
 - Made a welcome basket for witch, nice worked goods, perfume, etc.
 - Stopped by the mines met with Factor, Factor said try to get a trade deal.
 - By hunting camp, we saw a round table set with 8 seats, plates, salt, water.
 - Kai/Myki recognize this is a ritual of hospitality.
 - Party accepts the invite, meaning we must be peaceful.
 - Contract: Non-violence, leave when asked. 
 - After a moment, the hound approaches. Informs us the mistress will arrive 
at sunset. Swapped to old common. Malikai believes he is a Yeth-hound (evil).
Evil fey, typically contracted out to spellcasters or other fey.
 - Kai composed 'Witches get Bitches' song for introducing the two
 - Myki searches for clay, finds none really worthwhile.
 - Malikai & Five Timber set a trap outside the forest
 - Professor Pip tries to make a sigil/amulet for the gods
 - Witch arrives: Glorianna. Apologizes for young hunter death. Places pouch
 from fibrous plant stalk.
 - Witch summons the goblins to serve the food. Pale goblins. Fruit is quality,
 but it is a rushed meal.
 - Kai insights, Witch *knew* that it was a Pianta garden. Old days: 
 "To cultivate a garden is to walk with god."
 "You all are the first in a long, long time."
 She owns the domain of the forest.
 - Witch wants her cloak back (from Five Timber). We try to negotiate, she refuses.
 - Witch proposes a potential deal if we do her a favor. Wants 'Grace' (goblin) to die.
 A sorcerer living near 'her domain' born on the estival solstice. Lingers in the east forest.
 - We excuse ourselves with a hound guide. Travel 1 hr 15m deeper into the forest.
 - Hound keeps watching. We investigate the camp:
 Found goblin-sized utilities, group-activity, pawprints. Around 15-20 goblins.
 Found 'leader tent' with a hidden lantern with green flame (lantern of tracking).
 - Dressed like others. Goblin did return. Exact vector. Around 4-5 days ago.
 - Kai spoke with a tree, got the time & location. We follow, though he did teleport.
 Traveled about an hour.
 - "Follow no further" burn mark. "Witch will take your shadow, take your soul.
 Have faith and trust."
 - Party decides to camp the night.
 - Hound woke us up, heard the convo. They descend on us, we fight them off.
 We took out the hound and ~12 goblins. Grace gave us support fire.
 - Met Grace. Grace accepted a meeting. Goblin came here, fought her, lost. They
 burned the forest, she took from them what she lost. I command flames. Booyahlicker.
 She take shadow with golden shears. Old Booyah say she queen of darkness versus
 dawn queen in the ocean. He has army of 50-100. Shear break, no shadow.
 - We ask to go to camp. He took us to a cave system where we finished getting shut-eye.
 - Grace's terms: Free for the goblins, doesn't care if we take over the land.
 Goblins settled on continent to flee sundering. 
 - Much discussion was had. Not to be an unreliable narrator, but @__________@
 - Grace gave us a flaming focus. Found coiling grasp tattoo on goblin.
 - Furnian Ash Wand, Coiling Grasph Tattoo, 31g (bar tab), Lantern of Fey Tracking

#### Leads
February 17th, 2022

Kai, Myki, Malikai, Five Timbers, Pip, Ramos went to confront the witch who controls the domain of the forest.

 - The witch is expecting us to defeat 'Grace' (a sorcerer goblin) in exchange for the basis of a territorial agreement written in a contract between her forest and the colony. Lumber harvesting is a contentious topic. She also wanted her cloak (cloak of elven kind) returned. This deal may be off the table because we killed her favored hound (evil fey).
 - Witch owns golden shears that can tear shadows from beings, particularly goblins. She likely is involved with the fey.
 - Grace the goblin is preparing for war with 50-100 goblins ("many"). We struck an accord to attack the witch with him, in exchange the goblins will leave and never return, giving us free reign to the forest. He will need time to prepare.
 - There is a "Dawn Queen in the ocean", a powerful force that opposes the witch towards the east.

### Rocky I, II, III, and IV
- Decided to take cart along the coast towards the wreckage
- Pip created a staff holder inside the cart
- Scout: Would need cold resistance and a sturdy boat to cross bridge
- Party splits to investigate bridge. 
- Mali searches for goblins spying on us, found none.
- Aleks, Pip investigate the masonry and stonework: giant origin, cataclysm ruined
- Annabelle & Kai investigate creatures, found biolumniscent fish surrounding. 
Light decays the moment they pass. Likely magical.
Kai caught a crab and studied it for a bit. Pip caught necromancy and transmutation.
Divine sense gives a family-friendly source of necromancy.
- Freddi the Crab. Aleks built a wire mesh crate for him.
- Light show on the island disappated after dark.
- No issues in three watches. 
- Party arose, Five Timber caught fish, kai cooked 'em up, delicious. On the road!
- Found a 30 degree embedded ship in the coast. Unusual ship. No masts.
Ship is made of metal and wood. Crushed timbers. 110 ft long.
- Aleks uses hammers to open up the door. Ships look like its been salvaged.
- Noticed some creature has been eating metal inside the ship.
- Five Timber scouted ahead, found bones. Heard metal taps and echoes of metal sheer.
- Identified as dragonborn and humanoid bones. Killed by magical fire and acid.
Firestorm and Vitrolic Sphere cast within the ship.
- Found reinforced door with pinging sound. 
- Five timber and Aleks were able to get the door open. Pinging turned into an alarm.
The alarm was partly broken up. Sounds scary. Partly celestial.
- Construct shaped almost like a deer continuously spoke. Keeps stating message.
- Kai knows that this is a padagonia creature. Flying celestial city. Sentinel.
- Aleks tried to repair sentinel, it didn't attack, stated "do not enter the cargo bay"
- Party decided we needed access card. Moved forward. Five timber leads charge.
- Giant mouth earth monster with three eyes and tentacles.
- Ran into creature that is elemental. Been bitten. It ran away from Five Timber.
- Found a celestial sun & flying city with golden embroider. 
- Heward's Handy Haversack
    - 2 platinum rings (50g)
    - Diamond (300g)
    - scroll of gentle repose (unknown)
    - papers and books [book of prayer] (celestial)
- Found a draconic longsword (chromatic, EWW)
    - can be converted with downtime and money
- Pip talks to the Zorn, Ramos heals the Zorn. 
- Rocky was healed by Kai as well.
- Rocky told us about a Deepcrow in the mountain that attacked him 3-4 months ago.
- Bribe rocky with TWO platinum rings to bap the sentinel so we can loot
- Beam of radiant energy shoots out from sentinel to hit rocky
- We run into the room, chest is interwoven into reinforced room, lock is puzzlebox.
- With all our might (and resources), we pry the treasure off and put into bag of holding.
- MONKEY STRONG. Myki tackles it. 
- Ramos and Myki keep beam off of us, all of us book it. Rocky runs.
- Myki turns into a badger and dives into the sand hole blasted into the ship.
- Rocky came back to get his other snack.
- We ran home, Pip spent all his time trying to open the chest.
- He opened it the second he woke up from his 3 day coma, EUREKA!

#### Leads

February 10th, 2022

Kai, Myki, Malikai, Annabelle, Five Timbers, Pip, Aleksandra, Ramos went to investigate an ancient ship stuck in the sand on the coast. Recovered a puzzle treasure chest.

 - Bioluminescent fish and crabs appear to lose their essence upon death. Seems to be magical: necromancy with a divine sense of 'good.' Freddi the crab has been brought back as a specimen.
 - Bridge to the emerald isle has masonry likely created by giants and destroyed by either a natural or magical cataclysmic event. 
 - Ancient ship was full of humanoid and dragonborn bodies that were killed by a Firestorm and Vitriolic sphere. Ship appears to have been designed to fly. 
 - Found a sentinel construct that appears to be from the flying celestial city of Patagonia. Found a matching bag with a celestial sun and flying city embroidered into it. Orray within the puzzle box may have a beacon that can be used to track the city perhaps.
 - Rocky (an earth elemental) mentioned a Deepcrow that lurks in the mountains that attacked him 3-4 months ago. Pretty large.

### The Most Dangerous Game
 - Factor and his assistant gathered a group of us up to tell of missing hunters: Hugo, Martin, Bernard
 - Party traveled out towards the hunting camp
 - Party found tents that were rustling about, pigmies inside (green?)
 - Annabelle can speak goblin, found out they were goblins
 - Goblins were a mix of pale (without shadows) and standard
 - Party fought goblins, captured two, one escaped (whoops).
 - Captured one interrogated: ritual where goblins and humans and light sacrificed to a witch
 - Goblin led us towards camp, we took a different bloody trail towards the 'gifts'
 - Asked a tree (who knew Kai's grandma) why footprints went from 3 to 2 humans, pointed into woods
 - Found body of Hugo RIP
 - We met a tabaxi rogue named Five Timbers scouting for hunters as well
 - Found the predernatural darkness that grew denser, light ineffective.
 - Found an altar with a bunch of strange sylvan moss
 - Erviki: Shade Garden found in ancient pianta
 - Kai & Myki read the pianta tablet, get a sense that this is corrupted
 - Shadows (ripped from goblins) attacked Five Timbers
 - Aleks used crowbar to pry free the two hunters
 - Group runs, Annabelle sees shadow dog lurking with us
 - Goblins charge us, they transform into some creature after death, we defeated 'em
 - Found an amulet worth 250g and cloak of elvenkind
 - Brought the two wounded hunters and Hugo's body back to town

#### Leads
January 27th, 2022

Kai, Myki, Malikai, Annabelle, Five Timbers, Pip, Aleksandra tried to find missing hunters.

 - Found a dimeritium amulet on a goblin that may be worth researching.
 - Discovered an altar to a witch that can rip shadows from beings living in the preternatural darkness. Follow the bloody path past the hunter camp.
 - Sign 'Erviki: Shade Garden' in Pianti, suggests ancient pianta garden involved with the strange darkness.
 - Known goblin encampment placating this witch with gifts and sacrifices along the non-bloody path at the fork.
 - On death, most goblins sprouted cursed(?) mushrooms that sang in sylvan. Some sprouted chitinous armor and came back as undead. Also weak to fire.
