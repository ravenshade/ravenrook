# Government Information

## Officals
- Factor: Sir Wlliam Helm
- Assistant to the Factor: Henry Johnson

## Colony Coffers

- 15,972 Gold (calculated 03/05/2023)

## William's Wallet

- 500 gp (7/21/2022)
- (500)gp (9/29/2022: Invested with Kai)
- 530gp (03/05/2223 Investment with Kai repayment)

## Town Improvements

- Roads 1 (Purchased 7/05/2022: -2,000gp)
- Mines 2 (Purchased 6/10/2022: -7,500gp)
- Docks 2 (Purchased 5/24/2022: -5,000gp)
- Library (Purchased 7/27/2022: -1,500gp, 1,000gp donation)
- Emerald Island Ferry (Purchased 9/06/2022: -2,000gp)
- Location Prospecting Round 1 (Purchased 9/06/2022: -500gp)
- Pirate Hunting Bounty (half paid to party 9/29/2022: -1,000gp)
- Reclaimed Pirate Ship (Purchased from party 9/29/2022: -1,500gp)
- Invest in Colony Infrastructure (Purchased 9/30/2022: -2,000gp)
- Location Prospecting Round 2 (Purchased 10/07/2022: -500gp)
- Museum (Purchased 12/02/2022: -2000gp)
- Stable and Raven's Rookery (Purchased 1/19/2023: -5000gp)

## Debts & Expectations

- No Debts

## Laws and Ordinances

- No witches, kill all witches.
