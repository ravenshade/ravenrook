[[_TOC_]]

## Lore

Primary lore for the world and Ravenrook.

## New Continent
 - Mercantile colony of a corporation. Nationally unaffiliated. Merchant guild.
 - Only the bare necessities built out.
 - Not yet proven economically viable.
 - Myriad backgrounds of colonists: draft-dodgers, war effort, religious fanaticism

### Land
 - Mountains: hot springs, geothermal areas
 - Desert: On the other side of the mountains, has thick dark black clouds (that may be insect swarms)
 - Ocean: Wreckage of a ship to the north
 - Marshland: Stretches to the coast from the mountains
 - Weather is tempermental

### Resources
 - Rich veins of silver and dimeritium. Dimeritium is dangerous to mine because of its magical nature.
 - Ley lines seem severely diminished.

### Flora and Fauna
 - Giant flowers, seem to be passive, cross between rose and carnation
 - Plentiful flora around geothermal centers in and around mountains. Grows the deeper you go.
 - Ocean is rich with fish and crabs
 - No Snakes
 - Bestial dragons inhabit the mountains nearby
 - Emerald Isle has giant luminescent flying creatures
 - Glowing shrimp congregate around the emerald isle

### Natives
 - Small tribe of humanoid-like beings in the northern forest, near the lumbermill.

### Myths & Legends
 - Rift between magic users and non-magic users
   - Non-magic users crafted metal humanoids to help fight (warforged)
 - Great silver tower
   - Houses magical secrets, and a great sorceress (god, witch, etc)
   - Old fairy tale, shows up in different cultures (monomyth)
 - Great warrior kingdom in history books
   - Mentioned in relation to great warriors of legend, but no details are written, or found in archeological record. Perhaps they are from here
   - Famous song among bards about the warrior kingdom
   - Famous king and queen welding twin halves of an ancient blade
   - The kingdom was known for having magic being forbidden, the blades nullified magic
 - Cave drawings of Strange creatures
   - Dragon humanoid hybrids that don’t resemble dragon-born
 - “Hanging gardens” Evike
   - Exotic fruit that has never been tasted
   - Pianta myth, pianta’s garden of eden
 - Rumors that the dragon kingdoms existed in a different world
   - Progenitor dragons tore up the land to such a degree it became the moons. Can find traces of past lands there (on the moon?)
 - Legend of Patagonia, flying city of the clouds
   - Rumors of riches (gold and such)
 - Great tree
 - Avian paradise
   - Spread among the bird-folk, referred to as a bird tree
 - Rumors of a once great civilization among the dragon people
   - All dragons lived in unity
   - Earth opened beneath it, fell deep into the ground
   - Rumored to be on the day of the fabled triple eclipse
   - Each faction is in turmoil about it, blame each other
 - Belief in pangea
   - Might have been split by the cataclysm
 - Hospitality is in cultures that are familiar with dragons in case there is one in disguise visiting (greek style)
   - Social and religious risk for mistreating guests
 - Spooky cursed pirates that never show their face
   - Cursed by hospitality?


## World

### The War
 - On-going high magic war between major nations from the Dragon Homeland.
 - Dragon Homeland is fractured, split between types. Higher dragons often shapeshift to mingle with other races.
 - Iron March, a nation of Hobgoblins and goblins, are conquering the nations of animal races, forming them into substates.
 - War primarily occurs in the islands between the nations, causing undue harm to the Pianta people.
 - Enchantment magic is taboo, strong international and moral rules. Enchantment magic started the current war.

### Culture
 - There are three moons: one for each dragon type (metal, color, crystal). Holidays occur at different lunar events.
 - Special races (tieflings, aasimar, genasi, sorcerers) are born during lunar events.
 - Clerics are more powerful in areas where their god has more worshippers. Corporations provide lip service to gods.
 - Druids power is based on their circle (moon druid powerful at full moon, desert in the desert).
 - Primal Draconics are mainstay worshipped gods


### Religion

The Draconic Primals as known by name:

| Metallic | Draconic Primal Name |
|----------|----------------------|
| Golden   | Aurum                |
| Silver   | Argenti              |
| Copper   | Cuprum               |
| Bronze   | Aereus               |
| Brass    | Aeris                |

| Color | Draconic Primal Name |
|-------|----------------------|
| Black | Mávros               |
| White | Aspro                |
| Green | Prásinos             |
| Red   | Kókkino              |
| Blue  | Galanós              |

| Crystal   | Draconic Primal Name |
|-----------|----------------------|
| Moonstone | Mondstein            |
| Sapphire  | Saphir               |
| Emerald   | Smaragd              |
| Topaz     | Topas                |
| Amethyst  | Amethyst             |
